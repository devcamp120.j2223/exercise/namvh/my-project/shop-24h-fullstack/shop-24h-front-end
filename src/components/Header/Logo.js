import {
  Box,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import { Link } from "react-router-dom";
import { useState } from "react";
import fishingTackleLogo from "../../assets/images/Slide/logoFishing.jpg";

import MenuIcon from "@mui/icons-material/Menu";
import "../../App.css";
const Logo = () => {
  //đóng mở menu icon khi thu nhỏ màn hình
  const [anchorElNav, setAnchorElNav] = useState(null);
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <>
      <Box sx={{ display: { xs: "none", sm: "flex" }, flexGrow: 1 }}>
        <Grid container spacing={3} sx={{ flexGrow: 1, alignItems: "center" }}>
          <Grid item>
            <Link
              to="/"
              className="s24-cl2"
              style={{ textDecorationLine: "unset" }}
            >
              <Typography sx={{ fontSize: 20, fontWeight: "bold" }}>
                <img
                  alt="logo"
                  src={fishingTackleLogo}
                  height="55px"
                  width="55px"
                  style={{ border: "3px solid green", borderRadius: "50%" }}
                />{" "}
                &nbsp; FishingTackle
              </Typography>
            </Link>
          </Grid>
          <Grid item>
            <Link
              to="/products"
              className="s24-cl2"
              style={{ textDecorationLine: "unset" }}
            >
              <Typography sx={{ fontSize: 18 }}>Product</Typography>
            </Link>
          </Grid>
        </Grid>
      </Box>
      <Box
        sx={{
          flexGrow: 1,
          display: { xs: "flex", sm: "none" },
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <IconButton
          size="large"
          aria-controls="menu-appbar"
          aria-haspopup="true"
          onClick={handleOpenNavMenu}
          color="inherit"
        >
          <MenuIcon />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorElNav}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          keepMounted
          transformOrigin={{ vertical: "top", horizontal: "right" }}
          open={Boolean(anchorElNav)}
          onClose={handleCloseNavMenu}
          sx={{ display: { xs: "block", sm: "none" } }}
          PaperProps={{
            elevation: 0,
            sx: {
              overflow: "visible",
              filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
              mt: 1,
              bgcolor: "#eceff3",
              "&:before": {
                content: '""',
                display: "block",
                position: "absolute",
                top: 0,
                right: 14,
                width: 10,
                height: 10,
                bgcolor: "#eceff3",
                transform: "translateY(-50%) rotate(45deg)",
                zIndex: 0,
              },
            },
          }}
        >
          <MenuItem>
            <Link
              to="/products"
              className="s24-cl5"
              style={{ textDecorationLine: "unset", fontSize: "12px" }}
            >
              PRODUCTS
            </Link>
          </MenuItem>
        </Menu>
      </Box>
    </>
  );
};
export default Logo;
