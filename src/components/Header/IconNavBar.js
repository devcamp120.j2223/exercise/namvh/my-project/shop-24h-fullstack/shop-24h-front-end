import {
  Box,
  Divider,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Badge,
  Button,
  List,
  ListItem,
  ListItemText,
  Typography,
  Avatar,
  ListItemAvatar,
  Grid,
} from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { auth } from "../../firebase";
import { useDispatch, useSelector } from "react-redux";
import { useSnackbar } from "notistack";

import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import LocalGroceryStoreOutlinedIcon from "@mui/icons-material/LocalGroceryStoreOutlined";
import LoginIcon from "@mui/icons-material/Login";
import Logout from "@mui/icons-material/Logout";
import MenuIcon from "@mui/icons-material/Menu";
import DeleteIcon from "@mui/icons-material/Delete";
import "../../App.css";
import fishingTackle from "../../assets/images/Slide/fishingtackle.jpg";

const IconNavBar = () => {
  const dispatch = useDispatch();

  const { productCartsArr, total } = useSelector(
    (reduxData) => reduxData.taskReducer
  );

  //đóng mở menu icon khi thu nhỏ màn hình
  const [anchorElNav, setAnchorElNav] = useState(null);
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  //đóng mở menu account khi click
  const [anchorElAcc, setAnchorElAcc] = useState(null);
  const openAcc = Boolean(anchorElAcc);
  const handleAccMenuClick = (event) => {
    setAnchorElAcc(event.currentTarget);
  };
  const handleAccMenuClose = () => {
    setAnchorElAcc(null);
  };

  //đóng mở menu cart khi click
  const [anchorElCart, setAnchorElCart] = useState(null);
  const openCart = Boolean(anchorElCart);
  const handleCartMenuClick = (event) => {
    setAnchorElCart(event.currentTarget);
  };
  const handleCartMenuClose = () => {
    setAnchorElCart(null);
  };

  //snack bar
  const { enqueueSnackbar } = useSnackbar();

  // user account
  const [user, setUser] = useState();
  //khai báo điều hướng
  const navigate = useNavigate();
  //logout account
  const logoutGoogle = () => {
    auth
      .signOut()
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        throw error;
      });
  };

  //xử lý sự kiện click button login
  const onClickLoginHandle = () => {
    navigate("/login");
  };
  //xử lý sự kiện click button logout
  const onClickLogoutHandle = () => {
    logoutGoogle();
  };

  //xử lý sự kiện click button delete trong giỏ hàng
  const onClickDeleteProductCartHandle = (value) => {
    dispatch({
      type: "DELETE_ITEM_CART",
      payload: {
        product: value,
      },
    });
  };

  //xử lý sự kiện thay đổi số lượng trong giỏ hảng
  const onProductCartQuantityChange = (quantittyChange, productElement) => {
    console.log("check value == ", productElement);
    console.log("check value quantity == ", quantittyChange);
    if (quantittyChange > 0) {
      dispatch({
        type: "UPDATE_ITEM_CART",
        payload: {
          product: {
            productId: productElement.productId,
            productName: productElement.productName,
            productImg: productElement.productImg,
            productPrice: productElement.productPrice,
            productQuantity: parseInt(quantittyChange),
            totalAdd: productElement.productPrice * parseInt(quantittyChange),
          },
        },
      });
    } else {
      enqueueSnackbar("product number is at least 1 value item", {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    let isCancelled = false;
    auth.onAuthStateChanged((result) => {
      if (!isCancelled) {
        setUser(result);
        if (result !== null) {
          dispatch({
            type: "ADD_USER",
            payload: {
              userCus: {
                fullName: result.displayName,
                email: result.email,
              },
            },
          });
        } else {
          dispatch({
            type: "ADD_USER",
            payload: {
              userCus: {
                fullName: "none",
                email: "none",
              },
            },
          });
        }
      }
    });
    return () => {
      isCancelled = true;
    };
  }, [productCartsArr, total]);
  return (
    <>
      <Box sx={{ display: { xs: "none", md: "flex" } }}>
        <IconButton size="large" aria-label="notifications">
          <NotificationsNoneIcon className="s24-cl2" />
        </IconButton>
        <IconButton
          size="large"
          aria-label="local grocery store"
          onClick={handleCartMenuClick}
          aria-controls={openCart ? "cart-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={openCart ? "true" : undefined}
        >
          <Badge badgeContent={productCartsArr.length} color="error">
            <LocalGroceryStoreOutlinedIcon className="s24-cl2" />
          </Badge>
        </IconButton>
        <Box sx={{ display: "flex" }}>
          <IconButton
            onClick={handleAccMenuClick}
            sx={{ ml: 2 }}
            aria-controls={openAcc ? "account-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={openAcc ? "true" : undefined}
            edge="end"
          >
            {user ? (
              <>
                <img
                  src={user.photoURL}
                  width={32}
                  height={32}
                  alt="avatar"
                  style={{ borderRadius: "50%" }}
                />
              </>
            ) : (
              <>
                <AccountCircleOutlinedIcon className="s24-cl2" />
              </>
            )}
          </IconButton>
        </Box>
      </Box>
      <Box
        sx={{
          flexGrow: 1,
          display: { xs: "flex", md: "none" },
          justifyContent: "flex-end",
        }}
      >
        <IconButton
          size="large"
          aria-controls="menu-appbar"
          aria-haspopup="true"
          onClick={handleOpenNavMenu}
          color="inherit"
        >
          <MenuIcon />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorElNav}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          keepMounted
          transformOrigin={{ vertical: "top", horizontal: "right" }}
          open={Boolean(anchorElNav)}
          onClose={handleCloseNavMenu}
          sx={{ display: { xs: "block", md: "none" } }}
          PaperProps={{
            elevation: 0,
            sx: {
              overflow: "visible",
              filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
              mt: 1,
              bgcolor: "#eceff3",
              "&:before": {
                content: '""',
                display: "block",
                position: "absolute",
                top: 0,
                right: 14,
                width: 10,
                height: 10,
                bgcolor: "#eceff3",
                transform: "translateY(-50%) rotate(45deg)",
                zIndex: 0,
              },
            },
          }}
        >
          <MenuItem>
            <IconButton size="large" aria-label="notifications">
              <NotificationsNoneIcon className="s24-cl2" />
            </IconButton>
          </MenuItem>
          <MenuItem>
            <IconButton
              size="large"
              aria-label="local grocery store"
              onClick={handleCartMenuClick}
              aria-controls={openCart ? "cart-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={openCart ? "true" : undefined}
            >
              <Badge badgeContent={productCartsArr.length} color="error">
                <LocalGroceryStoreOutlinedIcon className="s24-cl2" />
              </Badge>
            </IconButton>
          </MenuItem>
          <MenuItem>
            <IconButton
              onClick={handleAccMenuClick}
              aria-controls={openAcc ? "account-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={openAcc ? "true" : undefined}
            >
              {user ? (
                <>
                  <img
                    src={user.photoURL}
                    width={32}
                    height={32}
                    alt="avatar"
                    style={{ borderRadius: "50%" }}
                  />
                </>
              ) : (
                <>
                  <AccountCircleOutlinedIcon className="s24-cl2" />
                </>
              )}
            </IconButton>
          </MenuItem>
        </Menu>
      </Box>

      {/* menu dropdown account */}
      <Menu
        anchorEl={anchorElAcc}
        id="account-menu"
        open={openAcc}
        onClose={handleAccMenuClose}
        onClick={handleAccMenuClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1,
            bgcolor: "#eceff3",
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              left: "25%",
              width: 10,
              height: 10,
              bgcolor: "#eceff3",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "left", vertical: "top" }}
        anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
      >
        {user ? (
          <div key={0}>
            <MenuItem>
              <span className="s24-cl5"> {user.displayName}</span>
            </MenuItem>
            <MenuItem>
              <Link
                to="/myorder"
                className="s24-cl5"
                style={{ textDecorationLine: "unset" }}
              >
                My Order
              </Link>
            </MenuItem>
            <Divider />
            <MenuItem className="s24-cl5">
              <ListItemIcon onClick={onClickLogoutHandle}>
                <Logout fontSize="small" className="s24-cl5" />
                <span className="s24-cl5">Logout</span>
              </ListItemIcon>
            </MenuItem>
          </div>
        ) : (
          <div key={0}>
            <MenuItem>
              <ListItemIcon onClick={onClickLoginHandle}>
                <LoginIcon fontSize="small" className="s24-cl5" />
                <span className="s24-cl5">Login</span>
              </ListItemIcon>
            </MenuItem>
          </div>
        )}
      </Menu>

      {/* menu dropdown cart */}
      <Menu
        anchorEl={anchorElCart}
        id="cart-menu"
        open={openCart}
        onClose={handleCartMenuClose}
        PaperProps={{
          elevation: 0,
          sx: {
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            width: "100%",
            maxWidth: 350,
            maxHeight: 500,
            overflow: "visible",
            mt: 1,
            bgcolor: "#eceff3",
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: "48%",
              width: 10,
              height: 10,
              bgcolor: "#eceff3",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "center", vertical: "top" }}
        anchorOrigin={{ horizontal: "center", vertical: "bottom" }}
      >
        <List>
          {productCartsArr.length >= 1 ? (
            productCartsArr.map((productCartEle, index) => {
              return (
                <ListItem key={index} className="s24-cl4">
                  <ListItemAvatar>
                    <Avatar>
                      <img
                        src={fishingTackle}
                        width={36}
                        height={36}
                        alt="cart"
                        style={{ borderRadius: "50%", border: "0px" }}
                      />
                    </Avatar>
                  </ListItemAvatar>
                  <Grid container justifyContent="space-between">
                    <Grid item xs={6} alignItems="center">
                      <ListItemText
                        primary={
                          <Typography variant="inherit" noWrap maxWidth={200}>
                            {productCartEle.productName}
                          </Typography>
                        }
                        secondary={
                          <Typography variant="body2" noWrap>
                            {productCartEle.productPrice} $
                          </Typography>
                        }
                      />
                    </Grid>
                    <Grid item xs={2} textAlign="right" alignItems="center">
                      <input
                        type="number"
                        value={productCartEle.productQuantity}
                        className="form-control"
                        style={{ width: "75px" }}
                        onChange={(event) =>
                          onProductCartQuantityChange(
                            event.target.value,
                            productCartEle
                          )
                        }
                      />
                    </Grid>
                    <Grid item xs={2} textAlign="right">
                      <IconButton
                        onClick={() =>
                          onClickDeleteProductCartHandle(productCartEle)
                        }
                      >
                        <DeleteIcon fontSize="small" className="s24-cl4" />
                      </IconButton>
                    </Grid>
                  </Grid>
                </ListItem>
              );
            })
          ) : (
            <ListItem className="s24-cl4">
              <ListItemText>No product</ListItemText>
            </ListItem>
          )}
          <Divider className="s24-cl2" />
          <ListItem>
            <ListItemText>Total:</ListItemText>
            <Typography variant="h6" className="s24-cl5">
              {total} $
            </Typography>
          </ListItem>
          <ListItem sx={{ justifyContent: "flex-end" }}>
            <Link to="/cartpage" style={{ textDecorationLine: "none" }}>
              <Button id="s24-btn" variant="outlined" color="error">
                BUY
              </Button>
            </Link>
          </ListItem>
        </List>
      </Menu>
    </>
  );
};
export default IconNavBar;
