import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconNavBar from "./IconNavBar";
import Logo from "./Logo";
import "../../App.css";

const Header = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" sx={{ bgcolor: "#4a3a27" }} elevation={3}>
        <Toolbar sx={{ mx: 10 }}>
          <Logo />
          <IconNavBar />
        </Toolbar>
      </AppBar>
    </Box>
  );
};
export default Header;
