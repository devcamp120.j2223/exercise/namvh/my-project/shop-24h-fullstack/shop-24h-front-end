import { Box, Grid } from "@mui/material";
import InfoFooter1 from "./InfoFooter1";
import InfoFooter2 from "./InfoFooter2";
import InfoFooter3 from "./InfoFooter3";
import SocialFooter from "./SocialFooter";
import "../../App.css";

const Footer = () => {
  return (
    <footer className="mt-5 s24-bg5 s24-cl4">
      <Grid container padding={5}>
        <Grid item xs={12} sm={12} md={8} padding={1}>
          <Box sx={{ display: "flex", justifyContent: "space-evenly" }}>
            <Grid container spacing={1}>
              <InfoFooter1 />
              <InfoFooter2 />
              <InfoFooter3 />
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={4} padding={1}>
          <SocialFooter />
        </Grid>
      </Grid>
    </footer>
  );
};
export default Footer;
