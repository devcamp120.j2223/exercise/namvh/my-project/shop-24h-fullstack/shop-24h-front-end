import { Button, Container } from "@mui/material";
import "../../App.css";

const ViewAll = ({ buttonContent, buttonClick }) => {
  return (
    <Container sx={{ textAlign: "center" }}>
      <Button
        variant="contained"
        size="large"
        id="s24-btn"
        onClick={() => buttonClick()}
      >
        {buttonContent ? "Show Less" : "Show All"}
      </Button>
    </Container>
  );
};
export default ViewAll;
