import { Carousel } from "react-bootstrap";
import { Container, Grid, Typography, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";

import vanford from "../../assets/images/Slide/vanford.jpg";
import stella from "../../assets/images/Slide/stella.jpg";
import saltiga from "../../assets/images/Slide/saltiga.jpg";
import zenaqTobizo from "../../assets/images/Slide/3000.jpg";

const CarouselContent = () => {
  const navigate = useNavigate();
  const navigateToProducts = () => {
    navigate("/products");
  };
  return (
    <>
      <div style={{ marginTop: "75px" }}>
        <Container>
          <Carousel
            cols={2}
            rows={1}
            gap={10}
            style={{
              textAlign: "center",
              color: "black",
              fontSize: "medium",
              fontWeight: "600",
            }}
          >
            <Carousel.Item>
              <img
                width="1200px"
                height="600px"
                src={vanford}
                alt="vanford"
                onClick={navigateToProducts}
              />
            </Carousel.Item>

            <Carousel.Item>
              <img
                width="1200px"
                height="600px"
                src={stella}
                alt="stella"
                onClick={navigateToProducts}
              />
            </Carousel.Item>

            <Carousel.Item>
              <img
                width="1200px"
                height="600px"
                src={saltiga}
                alt="saltiga"
                onClick={navigateToProducts}
              />
            </Carousel.Item>

            <Carousel.Item>
              <img
                width="1200px"
                height="600px"
                src={zenaqTobizo}
                alt="tobizo"
                onClick={navigateToProducts}
              />
            </Carousel.Item>
          </Carousel>
        </Container>
      </div>
    </>
  );
};

export default CarouselContent;
