import { Container, Box, Card, Grid, Typography, Stack } from "@mui/material";
import { Link } from "react-router-dom";
import "../../App.css";

const LastestProducts = ({ productsData }) => {
  return (
    <Container
      sx={{ marginY: 0, paddingTop: 10 }}
      className="s24-backgroundPage"
    >
      <Typography
        className="s24-cl1"
        variant="h5"
        textAlign="center"
        sx={{ fontWeight: "bold" }}
      >
        LASTEST PRODUCTS
      </Typography>
      <Grid container spacing={3} marginY={3}>
        {productsData ? (
          productsData.map((product, index) => {
            return (
              <Grid item xs={12} sm={4} md={3} key={index}>
                <Card
                  className="s24-bg3"
                  style={{ border: "1px solid lightBlue" }}
                >
                  <Box
                    sx={{
                      pt: "30%",
                      position: "relative",
                      textAlign: "center",
                    }}
                  >
                    <img
                      src={product.imageUrl}
                      alt={product.name}
                      width="200"
                      height="140"
                    />
                  </Box>
                  <Stack spacing={2} sx={{ p: 3 }} textAlign="center" mt={4}>
                    <Link
                      to={"/products/" + product._id}
                      style={{ textDecorationLine: "none" }}
                      className="s24-cl1"
                    >
                      <Typography variant="body1" fontWeight="bold" noWrap>
                        {product.name}
                      </Typography>
                    </Link>
                    <Stack direction="row" justifyContent="center">
                      <Typography variant="body1" className="s24-cl5">
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{
                            color: "text.disabled",
                            textDecoration: "line-through",
                            fontSize: "18px",
                            fontWeight: "400",
                          }}
                        >
                          {product.buyPrice} $
                        </Typography>
                        &nbsp; &nbsp;
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{
                            color: "green",
                            fontSize: "18px",
                            fontWeight: "600",
                          }}
                        >
                          {product.amount} $
                        </Typography>
                      </Typography>
                    </Stack>
                  </Stack>
                </Card>
              </Grid>
            );
          })
        ) : (
          <></>
        )}
      </Grid>
    </Container>
  );
};
export default LastestProducts;
