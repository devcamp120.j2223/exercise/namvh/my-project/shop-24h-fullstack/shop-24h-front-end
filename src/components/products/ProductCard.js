import {
  Card,
  Grid,
  Typography,
  CardActionArea,
  CardContent,
  Box,
  Stack,
} from "@mui/material";

import { useNavigate, Link } from "react-router-dom";
import "../../App.css";

const ProductCard = ({ productsData }) => {
  const navigate = useNavigate();
  const moveToDetailInfo = (product) => {
    navigate(`/products/${product._id}`);
  };

  return (
    <>
      {productsData ? (
        productsData.map((product, index) => {
          return (
            <Grid item xs={12} sm={6} md={4} key={index} mt={5}>
              <Card
                sx={{
                  maxWidth: 345,
                  height: "100%",
                  textAlign: "center",
                  padding: "10px",
                }}
                onClick={() => moveToDetailInfo(product)}
              >
                <CardActionArea>
                  <CardContent>
                    <img
                      src={product.imageUrl}
                      alt={product.name}
                      width="auto"
                      height="140"
                    />
                    <Typography gutterBottom variant="h5" component="div">
                      {product.name}
                    </Typography>

                    <Typography variant="body2" color="text.secondary" mt={4}>
                      {product.description}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          );
        })
      ) : (
        <></>
      )}
    </>
  );
};
export default ProductCard;
