import {
  Box,
  Stack,
  Button,
  Checkbox,
  FormGroup,
  IconButton,
  Typography,
  FormControlLabel,
  InputBase,
  Grid,
  Paper,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { useEffect, useState } from "react";
import "../../App.css";

const ProductFilter = ({ getFilter }) => {
  //khai báo các biến filter
  const [findName, setFindName] = useState("");
  const [typeChecked, setTypeChecked] = useState([]);
  const [minPromotionPrice, setMinPromotionPrice] = useState(0);
  const [maxPromotionPrice, setMaxPromotionPrice] = useState("");
  //lấy dữ liệu productTypes từ server set mảng check box Categories
  const [typesData, setTypesData] = useState([]);
  const getTypes = async () => {
    const response = await fetch("http://localhost:8000/ProductTypes");
    const data = await response.json();
    return data;
  };

  //Hàm xử lý sự kiện khi chọn check box
  //input: giá trị khi bấm
  //output: mảng typeChecked chứa các giá trị đang tích chọn
  const onChangeCheckBoxTypeHandle = (value) => {
    //xét giá trị đã nằm trong mảng typeChecked hiện tại không
    if (typeChecked.includes(value)) {
      //giá trị đã có trong mảng, xóa giá trị ra khỏi mảng
      let newArr = typeChecked.filter((ele) => {
        return ele !== value;
      });
      setTypeChecked(newArr);
    } else {
      //giá trị chưa có trong mảng, add giá trị vào mảng
      setTypeChecked([...typeChecked, value]);
    }
  };

  //Hàm xử lý sự kiện khi click filter
  //output: string chứa param condition gửi lên url
  const onClickBtnFilterHandle = () => {
    let stringType = "";
    typeChecked.forEach((ele, index) => {
      stringType = stringType + "&type=" + ele;
    });
    let stringFilter =
      "name=" +
      findName +
      "&min=" +
      minPromotionPrice +
      "&max=" +
      maxPromotionPrice +
      stringType;
    getFilter(stringFilter);
  };

  useEffect(() => {
    getTypes()
      .then((data) => {
        setTypesData(data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <>
      <Stack spacing={3}>
        <div>
          <Typography
            variant="subtitle1"
            gutterBottom
            fontWeight="bold"
            className="s24-cl5"
          >
            Product Name
          </Typography>
          <Grid container>
            <Paper
              component="form"
              sx={{
                p: "0px 10px",
                display: "flex",
                alignItems: "center",
                width: "100%",
              }}
            >
              <InputBase
                sx={{ flex: 1 }}
                placeholder="Search Product Name"
                value={findName}
                onChange={(event) => setFindName(event.target.value)}
              />
              <IconButton
                type="submit"
                sx={{ p: "10px" }}
                aria-label="search"
                disabled
              >
                <SearchIcon />
              </IconButton>
            </Paper>
          </Grid>
        </div>

        <div>
          <Typography
            variant="subtitle1"
            gutterBottom
            fontWeight="bold"
            className="s24-cl5"
          >
            Price
          </Typography>
          <Grid container textAlign="center">
            <Grid item xs={5}>
              <input
                min={0}
                type="number"
                value={minPromotionPrice}
                className="form-control"
                style={{ width: "100%" }}
                onChange={(event) => setMinPromotionPrice(event.target.value)}
              />
            </Grid>
            <Grid item xs={2}>
              -
            </Grid>
            <Grid item xs={5}>
              <input
                min={0}
                type="number"
                value={maxPromotionPrice}
                className="form-control"
                style={{ width: "100%" }}
                onChange={(event) => setMaxPromotionPrice(event.target.value)}
              />
            </Grid>
          </Grid>
        </div>

        <div>
          <Typography
            variant="subtitle1"
            gutterBottom
            fontWeight="bold"
            className="s24-cl5"
          >
            Categories
          </Typography>
          <FormGroup>
            {typesData.map((element, index) => {
              return (
                <FormControlLabel
                  key={index}
                  control={
                    <Checkbox
                      value={element._id}
                      onChange={() => onChangeCheckBoxTypeHandle(element._id)}
                    />
                  }
                  label={element.name}
                  className="s24-cl4"
                />
              );
            })}
          </FormGroup>
        </div>
      </Stack>

      <Box sx={{ marginTop: 2 }}>
        <Button
          fullWidth
          size="large"
          id="s24-btn"
          variant="contained"
          onClick={onClickBtnFilterHandle}
        >
          Filter
        </Button>
      </Box>
    </>
  );
};
export default ProductFilter;
