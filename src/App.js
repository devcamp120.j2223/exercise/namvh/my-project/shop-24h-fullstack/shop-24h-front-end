import "bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";
import CartPage from "./pages/CartPage";
import Checkout from "./pages/Checkout";
import Home from "./pages/Home";
import Login from "./pages/Login";
import MyOrder from "./pages/MyOrder";
import ProductInfo from "./pages/ProductInfo";
import ProductList from "./pages/ProductList";

function App() {
  return (
    <div>
      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route exact path="/products" element={<ProductList />}></Route>
        <Route exact path="/login" element={<Login />}></Route>
        <Route exact path="/cartpage" element={<CartPage />}></Route>
        <Route exact path="/checkout" element={<Checkout />}></Route>
        <Route exact path="/myorder" element={<MyOrder />}></Route>
        <Route
          exact
          path="/products/:productId"
          element={<ProductInfo />}
        ></Route>
        <Route exact path="*" element={<Home />}></Route>
      </Routes>
    </div>
  );
}

export default App;
