import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import BreadCrumb from "../components/BreadCrumb";

import ProductDetailInfo from "../components/product-detail/ProductDetailInfo";
import ProductDetailDescription from "../components/product-detail/ProductDetailDescription";
import RelatedProducts from "../components/product-detail/RelatedProducts";

import { Container } from "@mui/material";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import "../App.css";
//--------------------

const ProductInfo = () => {
  //lấy id từ param
  const { productId } = useParams();
  console.log(">> check productId", productId);
  //khai báo fetch
  const fetchApi = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  };
  //khai báo thông tin sản phẩm
  const [productDetailData, setProductDetailData] = useState({
    buyPrice: 0,
    description: "",
    imageUrl: "logo.jpg",
    name: "",
    promotionPrice: 0,
    type: "",
  });
  //khai báo biến để lấy type name theo type id
  const [typeName, setTypeName] = useState("");

  //khai báo biến lưu thông tin cho mảng relatedProduct data
  const [relatedProductData, setRelatedProductData] = useState([]);

  //khai báo mảng breadcrumb
  const [breadCrumbList, setBreadCrumbList] = useState([
    {
      name: "",
      url: "",
    },
  ]);

  useEffect(() => {
    //di chuyển về đầu trang
    window.scroll({ top: 0, behavior: "smooth" });
    //hàm getProductById
    fetchApi("http://localhost:8000/products/" + productId)
      .then((data) => {
        let getProductByIdResult = data.data;
        setProductDetailData(getProductByIdResult);
        //set breadcrumb
        let breadCrumbProductDetail = [
          {
            name: "Home",
            url: "/",
          },
          {
            name: "Products",
            url: "/products",
          },
          {
            name: getProductByIdResult.name,
            url: "#",
          },
        ];
        setBreadCrumbList(breadCrumbProductDetail);
        //set type name
        let typeId = getProductByIdResult.type;
        fetchApi("http://localhost:8000/ProductTypes/" + typeId)
          .then((typesData) => {
            setTypeName(typesData.data.name);
          })
          .catch((error) => {
            throw error;
          });
        //set related product data
        fetchApi("http://localhost:8000/products?type=" + typeId)
          .then((dataFilterType) => {
            //lọc sản phẩm hiện tại ra khỏi mảng related
            let result = dataFilterType.data.filter((ele) => {
              return ele._id !== getProductByIdResult._id;
            });
            setRelatedProductData(result);
          })
          .catch((error) => {
            throw error;
          });
      })
      .catch((error) => {
        throw error;
      });
  }, [productId]);

  return (
    <div className="s24-backgroundPage">
      <Header />
      <Container sx={{ marginTop: 8, paddingTop: 2 }}>
        <BreadCrumb breadCrumbList={breadCrumbList} />
      </Container>
      <Container sx={{ marginTop: 2 }}>
        <ProductDetailInfo
          productDetailData={productDetailData}
          typeName={typeName}
        />
        <ProductDetailDescription productDetailData={productDetailData} />
        <RelatedProducts relatedProductData={relatedProductData} />
      </Container>
      <Footer />
    </div>
  );
};
export default ProductInfo;
