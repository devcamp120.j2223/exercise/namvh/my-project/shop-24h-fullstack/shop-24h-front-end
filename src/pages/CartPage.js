import {
  Button,
  Container,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import "bootstrap/dist/css/bootstrap.min.css";
import BreadCrumb from "../components/BreadCrumb";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import DeleteIcon from "@mui/icons-material/Delete";
import { useSnackbar } from "notistack";
import "../App.css";

const CartPage = () => {
  const dispatch = useDispatch();
  const { productCartsArr, total, userCus } = useSelector(
    (reduxData) => reduxData.taskReducer
  );

  //khai báo điều hướng
  const navigate = useNavigate();

  //khai báo mảng breadcrumb
  const breadCrumbList = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "Cart",
      url: "#",
    },
  ];

  //snack bar
  const { enqueueSnackbar } = useSnackbar();
  //khai báo fetch
  const fetchApi = async (urlApi, bodyApi) => {
    const response = await fetch(urlApi, bodyApi);
    const data = await response.json();
    return data;
  };

  //xử lý sự kiện click button delete trong giỏ hàng
  const onClickDeleteProductCartHandle = (value) => {
    dispatch({
      type: "DELETE_ITEM_CART",
      payload: {
        product: value,
      },
    });
  };

  //xử lý sự kiện thay đổi số lượng trong giỏ hảng
  const onProductCartQuantityChange = (quantittyChange, productElement) => {
    if (quantittyChange > 0) {
      dispatch({
        type: "UPDATE_ITEM_CART",
        payload: {
          product: {
            productId: productElement.productId,
            productName: productElement.productName,
            productImg: productElement.productImg,
            productPrice: productElement.productPrice,
            productQuantity: parseInt(quantittyChange),
            totalAdd: productElement.productPrice * parseInt(quantittyChange),
          },
        },
      });
    } else {
      enqueueSnackbar(
        "Product number is allways be more 0. when do not buy it, please remove item!",
        { variant: "error" }
      );
    }
  };

  //Hàm xử lý sự kiện click button Order
  const onClickOrderHandle = () => {
    if (userCus.email === "none") {
      navigate("/login");
    } else {
      fetchApi("http://localhost:8000/Customers")
        .then((data) => {
          data.data.forEach((element) => {
            if (element.email === userCus.email) {
              dispatch({
                type: "ADD_USER_ID",
                payload: {
                  userId: element._id,
                },
              });
            }
          });
          navigate("/checkout");
        })
        .catch((err) => {
          throw err;
        });
    }
  };

  return (
    <div className="s24-backgroundPage">
      <Header />
      <Container sx={{ marginTop: 8, paddingTop: 2 }}>
        <BreadCrumb breadCrumbList={breadCrumbList} />
      </Container>
      <Container sx={{ marginTop: 2 }}>
        <div className="p-5 mb-5 bg-light rounded-3">
          <h3 className="text-center s24-cl1">YOUR ORDER</h3>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="cart table">
              <TableHead>
                <TableRow className="s24-bg4 bg-gradient">
                  <TableCell align="center" colSpan={3}>
                    <Typography className="s24-cl3">PRODUCT</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography className="s24-cl3">PRICE</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography className="s24-cl3">QUANTITY</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography className="s24-cl3">SUBTOTAL</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {productCartsArr.length >= 1 ? (
                  productCartsArr.map((productCartEle, index) => {
                    return (
                      <TableRow key={index}>
                        <TableCell align="left">
                          <IconButton
                            onClick={() =>
                              onClickDeleteProductCartHandle(productCartEle)
                            }
                          >
                            <DeleteIcon fontSize="small" />
                          </IconButton>
                        </TableCell>
                        <TableCell align="right">
                          <img
                            src={productCartEle.productImg}
                            className="img-thumbnail"
                            alt="product"
                            height="80px"
                            width="80px"
                          />
                        </TableCell>
                        <TableCell align="left">
                          {productCartEle.productName}
                        </TableCell>
                        <TableCell align="center">
                          {productCartEle.productPrice} $
                        </TableCell>
                        <TableCell align="center">
                          <input
                            type="number"
                            value={productCartEle.productQuantity}
                            className="form-control"
                            style={{ width: "75px" }}
                            onChange={(event) =>
                              onProductCartQuantityChange(
                                event.target.value,
                                productCartEle
                              )
                            }
                          />
                        </TableCell>
                        <TableCell align="center">
                          {productCartEle.productPrice *
                            productCartEle.productQuantity}{" "}
                          $
                        </TableCell>
                      </TableRow>
                    );
                  })
                ) : (
                  <TableRow>
                    <TableCell align="center">no product</TableCell>
                  </TableRow>
                )}
                <TableRow className="s24-bg4 bg-gradient">
                  <TableCell align="left" colSpan={5}>
                    <Typography className="s24-cl3 fw-bold">TOTAL</Typography>
                  </TableCell>
                  <TableCell align="center">
                    <Typography className="s24-cl3 fw-bold">
                      {total} $
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <div className="float-end my-2">
            <Button
              variant="contained"
              id="s24-btn"
              size="large"
              onClick={onClickOrderHandle}
            >
              ORDER
            </Button>
          </div>
        </div>
      </Container>
      <Footer />
    </div>
  );
};

export default CartPage;
