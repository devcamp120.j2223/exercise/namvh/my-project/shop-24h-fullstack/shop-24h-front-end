import {
  Button,
  Container,
  Grid,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextareaAutosize,
  TextField,
} from "@mui/material";
import "bootstrap/dist/css/bootstrap.min.css";
import BreadCrumb from "../components/BreadCrumb";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import { useDispatch, useSelector } from "react-redux";

import { Form } from "reactstrap";
import { useEffect, useState } from "react";
import { useSnackbar } from "notistack";
import "../App.css";
import { useNavigate } from "react-router-dom";

const Checkout = () => {
  const dispatch = useDispatch();
  const { productCartsArr, total, userCus } = useSelector(
    (reduxData) => reduxData.taskReducer
  );
  const navigate = useNavigate();

  //khai báo mảng breadcrumb
  const breadCrumbList = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "Checkout",
      url: "#",
    },
  ];

  //snack bar
  const { enqueueSnackbar } = useSnackbar();

  //khai báo biến
  const [fullName, setFullName] = useState(userCus.fullName);
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState(userCus.email);
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [note, setNote] = useState("");

  //khai báo fetch
  const fetchApi = async (urlApi, bodyApi) => {
    const response = await fetch(urlApi, bodyApi);
    const data = await response.json();
    return data;
  };

  //hàm xử lý sự kiện khi click button place order
  const onBtnPlaceOrderClickhander = (event) => {
    event.preventDefault();
    //B1: thu thập dữ liệu
    let userOrder = {
      fullName: fullName.trim(),
      phone: phone.trim(),
      email: email.trim(),
      address: address.trim(),
      city: city.trim(),
      country: country.trim(),
    };
    //B2: validate ( đơn hàng phải có sản phẩm)
    if (total > 0) {
      //B3: xử lý dữ liệu
      //Tìm email có trùng email đã có trên database?
      fetchApi("http://localhost:8000/Customers")
        .then((data) => {
          let idFound = null;
          data.data.forEach((element) => {
            if (element.email === userOrder.email) {
              idFound = element._id;
              dispatch({
                type: "ADD_USER_ID",
                payload: {
                  userId: element._id,
                },
              });
            }
          });
          if (idFound !== null) {
            updateUserAnSendOrder(idFound, userOrder);
          } else {
            createUserAndSendOrder(userOrder);
          }
        })
        .catch((err) => {
          throw err;
        });
    } else {
      enqueueSnackbar(
        "there is nothing in your cart! at least please choose an item!",
        { variant: "error" }
      );
    }
  };

  //hàm xử lý khi email người dùng đã tồn tại trên server
  const updateUserAnSendOrder = (userId, userData) => {
    let body = {
      method: "PUT",
      body: JSON.stringify(userData),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8000/Customers/" + userId, body)
      .then((response) => {
        if (response.status === "Internal server error") {
          throw new Error(response);
        }
      })
      .catch((err) => {
        enqueueSnackbar(
          "User info can not update, check route again or phone number and email has been used",
          { variant: "error" }
        );
      });
    createOrderOfCustomer(userId);
  };

  //hàm xử lý khi email người dùng chưa có trên server
  const createUserAndSendOrder = (userData) => {
    let body = {
      method: "POST",
      body: JSON.stringify(userData),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8000/Customers", body)
      .then((data) => {
        dispatch({
          type: "ADD_USER_ID",
          payload: {
            userId: data.data._id,
          },
        });
        createOrderOfCustomer(data.data._id);
      })
      .catch((err) => {
        enqueueSnackbar(
          "New user can not create, check route again or phone number and email has been used!",
          { variant: "error" }
        );
      });
  };

  //hàm tạo order trên api
  const createOrderOfCustomer = (userId) => {
    let orderData = {
      orderDetail: productCartsArr,
      cost: total,
      note: note,
    };
    let body = {
      method: "POST",
      body: JSON.stringify(orderData),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8000/Customers/" + userId + "/Orders", body)
      .then((data) => {
        enqueueSnackbar("Order has been created successfully !", {
          variant: "success",
        });
        //reset dữ liệu
        dispatch({
          type: "RESET_CART",
          payload: {},
        });
        setNote("");
        navigate("/myorder");
      })
      .catch((err) => {
        enqueueSnackbar("Error !!!Order have not been created yet", {
          variant: "error",
        });
      });
  };

  useEffect(() => {}, []);

  return (
    <div className="s24-backgroundPage">
      <Header />
      <Container sx={{ marginTop: 8, paddingTop: 2 }}>
        <BreadCrumb breadCrumbList={breadCrumbList} />
      </Container>
      <Container sx={{ marginTop: 5 }}>
        <Form onSubmit={onBtnPlaceOrderClickhander}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={7}>
              <Stack spacing={2}>
                <h4 className="s24-cl1">BILLING DETAILS</h4>
                <TextField
                  required
                  id="fullName"
                  name="fullName"
                  label="Full Name"
                  fullWidth
                  variant="standard"
                  value={fullName}
                  onChange={(event) => setFullName(event.target.value)}
                />
                <TextField
                  required
                  type="email"
                  id="email"
                  name="email"
                  label="Email"
                  fullWidth
                  variant="standard"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                />
                <TextField
                  required
                  type="number"
                  id="phone"
                  name="phone"
                  label="Phone number"
                  fullWidth
                  variant="standard"
                  value={phone}
                  onChange={(event) => setPhone(event.target.value)}
                />
                <TextField
                  required
                  id="address"
                  name="address"
                  label="Address"
                  fullWidth
                  variant="standard"
                  value={address}
                  onChange={(event) => setAddress(event.target.value)}
                />
                <Stack direction="row" spacing={2}>
                  <TextField
                    required
                    id="city"
                    name="city"
                    label="City"
                    fullWidth
                    variant="standard"
                    value={city}
                    onChange={(event) => setCity(event.target.value)}
                  />
                  <TextField
                    required
                    id="country"
                    name="country"
                    label="Country"
                    fullWidth
                    variant="standard"
                    value={country}
                    onChange={(event) => setCountry(event.target.value)}
                  />
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={5}>
              <h4 className="s24-cl1">YOUR ORDER</h4>
              <Grid container className="p-3 mb-3 rounded-3 s24-bg4">
                <Grid item xs={12}>
                  <TableContainer
                    component={Paper}
                    sx={{ marginBottom: 2, backgroundColor: "#eceff3" }}
                  >
                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell
                            align="center"
                            colSpan={2}
                            sx={{ fontWeight: "bold" }}
                          >
                            PRODUCT
                          </TableCell>
                          <TableCell align="center" sx={{ fontWeight: "bold" }}>
                            SUBTOTAL
                          </TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {productCartsArr.length >= 1 ? (
                          productCartsArr.map((productCartEle, index) => {
                            return (
                              <TableRow key={index}>
                                <TableCell align="left">
                                  {productCartEle.productName}
                                </TableCell>
                                <TableCell align="center">
                                  x&nbsp;{productCartEle.productQuantity}
                                </TableCell>
                                <TableCell
                                  align="center"
                                  sx={{ fontWeight: "bold" }}
                                >
                                  {productCartEle.productPrice *
                                    productCartEle.productQuantity}{" "}
                                  $
                                </TableCell>
                              </TableRow>
                            );
                          })
                        ) : (
                          <TableRow>
                            <TableCell align="center">no product</TableCell>
                          </TableRow>
                        )}
                        <TableRow>
                          <TableCell align="left" colSpan={2}>
                            TOTAL
                          </TableCell>
                          <TableCell
                            align="center"
                            sx={{
                              fontWeight: "bold",
                              color: "red",
                              fontSize: "18px",
                            }}
                          >
                            {total} $
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Grid>
                <Grid item xs={12}>
                  <TextareaAutosize
                    aria-label="note"
                    minRows={2}
                    placeholder="Note..."
                    style={{ width: "100%" }}
                    value={note}
                    onChange={(event) => {
                      setNote(event.target.value);
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    type="submit"
                    variant="contained"
                    id="s24-btn"
                    size="large"
                  >
                    PLACE ORDER
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Form>
      </Container>
      <Footer />
    </div>
  );
};

export default Checkout;
