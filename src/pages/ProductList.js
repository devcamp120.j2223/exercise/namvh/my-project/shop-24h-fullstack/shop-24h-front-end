import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Grid, Pagination } from "@mui/material";
import { useEffect, useState } from "react";

import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";

import BreadCrumb from "../components/BreadCrumb";
import ProductFilter from "../components/products/ProductFilter";
import ProductCard from "../components/products/ProductCard";
import SelectShowProductNumber from "../components/SelectShowProductNumber";
import "../App.css";
//------------------------

const ProductList = () => {
  //khai báo mảng breadcrumb
  const breadCrumbList = [
    {
      name: "Home",
      url: "/",
    },
    {
      name: "Products",
      url: "#",
    },
  ];

  const [noPage, setNoPage] = useState(0); //biến tổng số trang pagination
  const [page, setPage] = useState(1); //biến trang hiện tại pagination
  const [limit, setLimit] = useState(9); //biến giới hạn số sản phẩm hiển thị
  const [skip, setSkip] = useState(0); //biến lưu giá trị skip để hiển thị khi chuyển trang
  const [condition, setCondition] = useState(""); //biến lưu param url condition
  const [productsData, setProductsData] = useState([]); //biến lưu mảng sản phẩm hiển thị

  //url get dữ liệu
  const url =
    "http://localhost:8000/products?" +
    condition +
    "&limit=" +
    limit +
    "&skip=" +
    skip;

  //khai báo fetch api
  const getProduct = async (urlApi) => {
    const response = await fetch(urlApi);
    const data = await response.json();
    return data;
  };

  //Hàm xử lý khi bấm số trang pagination
  const onPageChange = (event, value) => {
    setPage(value);
    setSkip(limit * (value - 1));
  };

  //Hàm xử lý khi chọn select số sản phẩm hiển thị
  const changeSelectShowPageHandle = (value) => {
    setLimit(value);
    //reset page và skip
    setPage(1);
    setSkip(0);
  };

  //Hàm xử lý khi click filter
  const getFilter = (value) => {
    setCondition(value);
    //reset page và skip
    setPage(1);
    setSkip(0);
  };

  useEffect(() => {
    //di chuyển về đầu trang
    window.scroll({ top: 0, behavior: "smooth" });
    //set tổng số trang pagination
    getProduct("http://localhost:8000/products?" + condition)
      .then((data) => {
        setNoPage(Math.ceil(data.data.length / limit));
      })
      .catch((error) => {
        throw error;
      });
    //set dữ liệu hiển thị
    getProduct(url)
      .then((data) => {
        setProductsData(data.data);
      })
      .catch((error) => {
        throw error;
      });
  }, [limit, page, condition, url]);

  return (
    <div className="s24-backgroundPage">
      <Header />
      <Container sx={{ marginTop: 10, paddingTop: 3 }} maxWidth>
        <Grid container justifyContent="space-between" ml={1} mb={5}>
          <BreadCrumb breadCrumbList={breadCrumbList} />
          <SelectShowProductNumber
            showPage={limit}
            selectShowPageHandleProp={changeSelectShowPageHandle}
          />
        </Grid>
      </Container>
      <Container sx={{ marginTop: 2 }} maxWidth>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={3}>
            <ProductFilter getFilter={getFilter} />
          </Grid>
          <Grid item xs={12} sm={9}>
            <Grid container spacing={3}>
              <ProductCard productsData={productsData} />
            </Grid>
            <Grid
              container
              marginTop={3}
              marginBottom={3}
              justifyContent="flex-end"
            >
              <Pagination
                showFirstButton
                showLastButton
                count={noPage}
                defaultPage={1}
                page={page}
                onChange={onPageChange}
              />
            </Grid>
          </Grid>
        </Grid>
      </Container>
      <Footer />
    </div>
  );
};

export default ProductList;
